<?php

namespace Omnipay\Stripe\Message;

/**
 * Stripe List of Coupons Request
 */
class CouponsRequest extends AbstractRequest
{
    public function getData()
    {
        $data = array();

        return $data;
    }

    public function getHttpMethod()
    {
        return 'GET';
    }

    public function getEndpoint()
    {
        $endpoint = $this->endpoint.'/coupons';
        if($coupon = $this->getCoupon()) {
            $endpoint .= '/'. $coupon;
        }
        return $endpoint;
    }

    public function getCoupon()
    {
        return $this->getParameter('coupon');
    }

    public function setCoupon($value)
    {
        return $this->setParameter('coupon', $value);
    }
}
